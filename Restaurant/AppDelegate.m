//
//  AppDelegate.m
//  Restaurant
//
//  Created by Hafizur Rahman on 6/29/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"
#import <FacebookSDK/FacebookSDK.h>



@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [FBLoginView class];
    [FBProfilePictureView class];
    
    
    [self populateRecipeDetails];
    
    
    self.cartArray = [[NSMutableArray alloc]init];
    
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen] bounds]];
    
    NSString *nibName = @"MainViewController";
    
    if (UI_USER_INTERFACE_IDIOM()){
    
    }

        
    MainViewController *mainViewController = [[MainViewController alloc]initWithNibName:nibName bundle:[NSBundle mainBundle]];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:mainViewController];
    
    self.window.rootViewController = navController;
    
    [self.window makeKeyAndVisible];

    
    return YES;
}


- (void)populateRecipeDetails{
    
    self.foodArray = [[NSMutableArray alloc]init];

    ItemDetails *item1 = [[ItemDetails alloc]init];
    item1.title = @"Spaghetti Pollo/Pomodoro";
    item1.description = @"Ligera pasta con pechuga de pollo con jitomate cherry y trocitos de pechuga de pollo con un toque de albahaca.";
    item1.price = @"70";
    item1.image = [UIImage imageNamed:@"pasta.jpg"];
    item1.type = @"";
    item1.itemNo = 1;
    item1.availableTime = @"1:30 - 4:30 PM";

    [self.foodArray addObject:item1];
    
    
    ItemDetails *item2 = [[ItemDetails alloc]init];
    item2.title = @"Pollo Quinoa";
    item2.description = @"Pollo a la parrilla marinado en jugo de limón acompañado de ensalada de quinoa y vegetales.";
    item2.price = @"70";
    item2.image = [UIImage imageNamed:@"Chicken.jpg"];
    item2.type = @"Gluten-free, Sin lácteos";
    item2.itemNo = 2;
    item2.availableTime = @"1:30 - 4:30 PM";


    [self.foodArray addObject:item2];
    
    ItemDetails *item3 = [[ItemDetails alloc]init];
    item3.title = @"Ensalada Mandarina";
    item3.description = @"Mezcla de lechugas con vegetales verdes, col, aceitunas y gajos de mandarina.";
    item3.price = @"70";
    item3.image = [UIImage imageNamed:@"salad.jpg"];
    item3.type = @"Vegano, Gluten-free, Sin lácteos";
    item3.itemNo = 3;
    item3.availableTime = @"1:30 - 4:30 PM";
    
    [self.foodArray addObject:item3];
}


- (void)addToCart:(ItemDetails*)foodItem{
    
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:self.cartArray];
    
    BOOL isUpdatedOrInserted = NO;
    for (CartItem *item in array){
        
        if (item.foodItem == foodItem){
            
            item.counter += 1;
            NSInteger index = [self.cartArray indexOfObject:item];
            [self.cartArray replaceObjectAtIndex:index withObject:item];
            NSLog(@"Object Updated");
            isUpdatedOrInserted = YES;
        }
    }
    
    
    if (!isUpdatedOrInserted){

        NSLog(@"New Object inserted from else");

        CartItem *item = [[CartItem alloc]init];
        item.foodItem = foodItem;
        item.counter = 1;
        [self.cartArray addObject:item];
    }
}

- (void)removeFromCart:(ItemDetails*)foodItem{

    NSMutableArray *array = [NSMutableArray arrayWithArray:self.cartArray];
    
    for (CartItem *item in array){
        
        if (item.foodItem == foodItem){
            
            if (item.counter> 1){
                
                item.counter -= 1;
                NSLog(@"Deducted 1 item");
            }
            else{
                
                NSLog(@"Deleted last object");
                NSInteger index = [self.cartArray indexOfObject:item];
                [self.cartArray removeObjectAtIndex:index];
            }
        }
    }
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
    // You can add your app-specific url handling code here if needed
    
    return wasHandled;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
