//
//  AccountViewController.h
//  Restaurant
//
//  Created by Hafizur Rahman on 6/29/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "UserProfile.h"

@interface AccountViewController : UIViewController<FBLoginViewDelegate, UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>{
    
}



@end
