//
//  ViewController.m
//  Restaurant
//
//  Created by Hafizur Rahman on 6/29/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "MainViewController.h"
#import "AccountViewController.h"
#import "RecipeView.h"
#import "TitleView.h"
#import "AppDelegate.h"

#import "CheckoutViewController.h"


#define K_DOLLAR_TAG 7788
#define K_TIME_TAG 8844
#define K_INFO_BTN_TAG 8844

@interface MainViewController ()
@property (nonatomic, retain)IBOutlet UIScrollView *scrollView;

@property (nonatomic, retain) NSArray *imgArray;
@property (nonatomic, retain) NSArray *imgInfoArray;
@property (nonatomic, strong) IBOutlet UIView *titleView;
@property (nonatomic, strong) IBOutlet UILabel *titleViewLabel;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    
    /* add a button on the left side of navigation to signup or view account info*/
    
    self.navigationItem.titleView = _titleView;
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Accounts" style:UIBarButtonItemStylePlain target:self action:@selector(accountButtonPressed)];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Checkout" style:UIBarButtonItemStylePlain target:self action:@selector(checkOutPressed)];
    
    //self.navigationItem.rightBarButtonItem.enabled = NO;
    
    
    _imgArray = [[NSArray alloc]init];
    _imgArray = @[@"pasta.jpg",@"Chicken.jpg",@"salad.jpg"];
    
    _imgInfoArray = [[NSArray alloc]init];
    _imgInfoArray = @[@"Ligera pasta con pechuga de pollo con jitomate cherry y trocitos de pechuga de pollo con un toque de albahaca.",@"Pollo a la parrilla marinado en jugo de limón acompañado de ensalada de quinoa y vegetales.",@"Mezcla de lechugas con vegetales verdes, col, aceitunas y gajos de mandarina."];
    
    
    
    _scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    
    _scrollView.pagingEnabled = YES;
    _scrollView.directionalLockEnabled = YES; // not needed but helpful
    _scrollView.delegate = self;
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;

    
    AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    CGFloat viewWidth = self.view.frame.size.width;
    
    /* TODAY's RECIPE */
    
    UIScrollView *scrollViewPage1 = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, _scrollView.frame.size.width, _scrollView.frame.size.height-64)];
    scrollViewPage1.directionalLockEnabled = YES; // not needed but helpful

    
    TitleView *titleImageView1 = [[TitleView alloc]initWithFrame:CGRectMake(0, 0, viewWidth, 440)];
    titleImageView1.tag = 0;

    [titleImageView1 titleWithImage:[UIImage imageNamed:@"Ingredients1.jpg"] withDescription:@"Productos locales, ingredientes frescos, platillos deliciosos con el servicio más eficiente."];

    [scrollViewPage1 addSubview:titleImageView1];
    
    
    CGFloat initialYPos1 = 440;
    
    for (int i = 0 ; i < _imgArray.count; i++){
        
        RecipeView *recipeView1 = [[RecipeView alloc]initWithFrame:CGRectMake(0, initialYPos1, viewWidth, 260)];
        [recipeView1 addFoodMenu:(ItemDetails*)[delegate.foodArray objectAtIndex:i]];
        [scrollViewPage1 addSubview:recipeView1];
        recipeView1.tag = (i+1);
        initialYPos1 += 260;
    }
    
    [scrollViewPage1 setContentSize:CGSizeMake(_scrollView.contentSize.width, (260*_imgArray.count)+titleImageView1.frame.size.height)];
    
    [_scrollView addSubview:scrollViewPage1];
    
    
    
    /* TOMORROW's RECIPE */
    UIScrollView *scrollViewPage2 = [[UIScrollView alloc]initWithFrame:CGRectMake(0+viewWidth, 0, _scrollView.frame.size.width, _scrollView.frame.size.height-64)];
    
    TitleView *titleImageView2 = [[TitleView alloc]initWithFrame:CGRectMake(0, 0, viewWidth, 440)];
    titleImageView2.tag = 1;
    [titleImageView2 titleWithImage:[UIImage imageNamed:@"Ingredients1.jpg"] withDescription:@"Productos locales, ingredientes frescos, platillos deliciosos con el servicio más eficiente."];
    [scrollViewPage2 addSubview:titleImageView2];
    
    
    CGFloat initialYPos2 = 440;
    
    for (int i = 0 ; i < _imgArray.count; i++){
        
        RecipeView *recipeView2 = [[RecipeView alloc]initWithFrame:CGRectMake(0, initialYPos2, viewWidth, 260)];
        [recipeView2 addFoodMenu:(ItemDetails*)[delegate.foodArray objectAtIndex:i]];
        [scrollViewPage2 addSubview:recipeView2];
        recipeView2.tag = (i+1);
        initialYPos2 += 260;
    }
    
    [scrollViewPage2 setContentSize:CGSizeMake(_scrollView.contentSize.width, (260*_imgArray.count)+titleImageView2.frame.size.height)];
    
    [_scrollView addSubview:scrollViewPage2];
    
    
    [_scrollView setContentSize:CGSizeMake(viewWidth+viewWidth, _scrollView.contentSize.height)];
    [self.view addSubview:_scrollView];

}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat pageWidth = scrollView.frame.size.width; // you need to have a **iVar** with getter for scrollView
    float fractionalPage = scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    
    if (page == 0)
        self.titleViewLabel.text = @"LAUNCH TODAY";
    else
        self.titleViewLabel.text = @"LAUNCH TOMORROW";
    
    self.pageControl.currentPage = page;

}



- (void)checkOutPressed{

    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsKey];
    UserProfile *user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (user != NULL || user != nil){
    
        AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        
        
        for (CartItem *meal in delegate.cartArray){
            
            [meal.foodItem showDetails];
            NSLog(@"Item count: %ldi",(long)meal.counter);
        }
        
        
        CheckoutViewController *checkoutViewC = [[CheckoutViewController alloc]initWithNibName:@"CheckoutViewController" bundle:[NSBundle mainBundle]];
        
        [self.navigationController pushViewController:checkoutViewC animated:YES];
    }
    else{
    
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Registration" message:@"To check out you should have to signup through Email or Facebook or Gmail in Accounts section" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    
    }
}


/* To signup or view Account info Action */
- (void)accountButtonPressed{

    /* An object of AccountViewController to present as a modal View */
    AccountViewController *accountController = [[AccountViewController alloc]initWithNibName:@"AccountViewController" bundle:[NSBundle mainBundle]];
    
    UINavigationController *accountNavController = [[UINavigationController alloc]initWithRootViewController:accountController];
    [self presentViewController:accountNavController animated:YES completion:nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

/*
 1. Meal cost: $70.00
 2. Hours of delivery: Horario de entrega 1.30 - 4.30 pm (that's Spanish for hours of delivery)
 3. Delivery: Tiempo de entrega: 30 min. (that's in Spanish for Delivery time: 30 min.)
 4. Here are the new images with the descriptions:
 (Pasta image)
 Spaghetti Pollo/Pomodoro
 
 Ligera pasta con pechuga de pollo con jitomate cherry y trocitos de pechuga de pollo con un toque de albahaca.
 
 (Chicken image)
 Pollo Quinoa
 
 Pollo a la parrilla marinado en jugo de limón acompañado de ensalada de quinoa y vegetales.
 
 (Salad image)
 Ensalada Mandarina
 
 Mezcla de lechugas con vegetales verdes, col, aceitunas y gajos de mandarina.
 
 5. Remember that in the checkout, people will be able to order their meals for immediate deliver or schedule their deliver (for example if they order in the morning, they would want to schedule its delivery for 2.00 - 2.30 pm)

 */
