//
//  UserProfile.m
//  Restaurant
//
//  Created by Hafizur Rahman on 6/29/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "UserProfile.h"

@implementation UserProfile

@synthesize name;
@synthesize email;
@synthesize phone;
@synthesize gender;
@synthesize balance;
@synthesize registeredBy;
@synthesize isNewsLetter;
@synthesize creditCardNo;
@synthesize location;
@synthesize deliveryOption;
@synthesize password;

- (id)initWithCoder:(NSCoder *)decoder {
    
    self = [super init];
    
    if (!self) {
        return nil;
    }

    self.location = [decoder decodeObjectForKey:@"location"];

    self.name = [decoder decodeObjectForKey:@"name"];
    self.email = [decoder decodeObjectForKey:@"email"];
    self.phone = [decoder decodeObjectForKey:@"phone"];
    self.password = [decoder decodeObjectForKey:@"password"];
    self.gender = [decoder decodeObjectForKey:@"gender"];
    self.balance = [decoder decodeObjectForKey:@"balance"];
    self.registeredBy = (RegisteredService)[decoder decodeIntegerForKey:@"registeredBy"];
    self.isNewsLetter = [decoder decodeBoolForKey:@"isNewsletter"];
    self.creditCardNo = [decoder decodeObjectForKey:@"creditCardNo"];
    self.deliveryOption = [decoder decodeObjectForKey:@"deliveryOption"];

    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    
    [encoder encodeObject:self.location forKey:@"location"];

    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.phone forKey:@"phone"];
    [encoder encodeObject:self.gender forKey:@"gender"];
    [encoder encodeObject:self.password forKey:@"password"];
    [encoder encodeObject:self.balance forKey:@"balance"];
    [encoder encodeInteger:self.registeredBy forKey:@"registeredBy"];
    [encoder encodeBool:self.isNewsLetter forKey:@"isNewsletter"];
    [encoder encodeObject:self.creditCardNo forKey:@"creditCardNo"];
    [encoder encodeObject:self.deliveryOption forKey:@"deliveryOption"];

}

- (void)showUserProfile{
    
    NSLog(@"\nName: %@\nLocation: %@\nEmail: %@\nPhone: %@\nGender: %@\nBalance: %@\nService: %d\nisNewsLetter: %d\nCard Number: %@ \nDelivery: %@",self.name, self.location,self.email,self.phone, self.gender, self.balance, self.registeredBy, self.isNewsLetter, self.creditCardNo, self.deliveryOption);
}

@end
