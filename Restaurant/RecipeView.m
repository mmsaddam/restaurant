//
//  RecipeView.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/4/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "RecipeView.h"
#import "AppDelegate.h"


#define ORDER_START 7 // 7:00 AM
#define ORDER_END 16  // 4:00 PM

#define BUTTON_TAG 878
#define ADD_TO_CART_TAG 877

@interface RecipeView ()

@property (nonatomic, strong) UIImageView *recipeImageView;
@property (nonatomic) BOOL isInfoShowed;
@property (nonatomic, strong)AppDelegate *appDelegate;
@end

@implementation RecipeView

@synthesize recipeImageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];

    }
    return self;
}

- (void)addFoodMenu:(ItemDetails*)foodDeatils{


    recipeImageView = [[UIImageView alloc]initWithImage:foodDeatils.image];
    recipeImageView.frame = CGRectMake(0, 0, self.frame.size.width,self.frame.size.height-40);
    recipeImageView.contentMode = UIViewContentModeScaleAspectFit;
    recipeImageView.tag = self.tag;
    recipeImageView.userInteractionEnabled = YES;
    [self addSubview:recipeImageView];
    
    recipeImageView.layer.borderWidth = 1.0;
    recipeImageView.layer.borderColor = [[UIColor grayColor]CGColor];
    
    CGFloat yPos = recipeImageView.frame.size.height+5;

    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"America/Mexico_City"];
    NSLocale *locale = [NSLocale localeWithLocaleIdentifier:@"es_MX"];

    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    [formatter setTimeZone:timeZone];
    [formatter setDateFormat:@"YYYY-MM-DD HH:mm:ss a"];
    [formatter setLocale:locale];
    
    //NSLog(@"name: %@", [NSTimeZone knownTimeZoneNames]);

    NSLog(@"Current Time: %@ GMT",[NSDate date]);
    
    NSCalendar *cal = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    [cal setTimeZone:timeZone];
    [cal setLocale:locale];

    NSDateComponents *currentDateComps = [cal components:NSMinuteCalendarUnit|NSHourCalendarUnit
                               fromDate:[NSDate date]];
    [currentDateComps setTimeZone:timeZone];
    
    NSLog(@"Component Hour: %zd Compnent Min: %zd\n\n\n", currentDateComps.hour,currentDateComps.minute);

    if(1)//(currentDateComps.hour >= ORDER_START && currentDateComps.hour < ORDER_END)
    {
        
        UILabel *costLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, yPos, 70, 30)];
        costLabel.backgroundColor = [UIColor clearColor];
        costLabel.textColor = [UIColor redColor];
        costLabel.text = [NSString stringWithFormat:@"$%@",foodDeatils.price];
        [self addSubview:costLabel];
        
        UIButton *plusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        plusButton.frame = CGRectMake(90, yPos-5, 40, 40);
        [plusButton addTarget:self action:@selector(plusTapped:) forControlEvents:UIControlEventTouchUpInside];
        [plusButton setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
        [self addSubview:plusButton];
        
        UIImageView *circleImgView = [[UIImageView alloc]initWithFrame: CGRectMake(100, yPos-10, 50, 50)];
        circleImgView.image = [UIImage imageNamed:@"circle.png"];
        circleImgView.center = CGPointMake(self.center.x, circleImgView.center.y);
        [self addSubview:circleImgView];
        
        UILabel *countLabel = [[UILabel alloc]initWithFrame:CGRectMake(130, yPos, 40, 30)];
        countLabel.center = circleImgView.center;
        countLabel.textAlignment = NSTextAlignmentCenter;
        countLabel.font = [UIFont boldSystemFontOfSize:22];
        countLabel.backgroundColor = [UIColor clearColor];
        countLabel.textColor = [UIColor grayColor];
        countLabel.text = @"0";
        countLabel.tag = ADD_TO_CART_TAG;
        [self addSubview:countLabel];
        
        UIButton *minusButton = [UIButton buttonWithType:UIButtonTypeCustom];
        minusButton.frame = CGRectMake(190, yPos-5, 40, 40);
        [minusButton addTarget:self action:@selector(minusTapped:) forControlEvents:UIControlEventTouchUpInside];
        [minusButton setImage:[UIImage imageNamed:@"minus"] forState:UIControlStateNormal];
        
        [self addSubview:minusButton];
        
        UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [infoButton setImage:[UIImage imageNamed:@"infoButton.png"] forState:UIControlStateNormal];
        [infoButton setImage:[UIImage imageNamed:@"infoButtonHi.png"] forState:UIControlStateHighlighted];
        
        [infoButton setImage:[UIImage imageNamed:@"infoButtonHi.png"] forState:UIControlStateSelected];
        
        //infoButton.tintColor = [UIColor redColor];
        CGRect frame = CGRectMake(0, 0, 25, 25);
        frame.origin = CGPointMake(280, yPos+2);
        infoButton.frame = frame;
        infoButton.tag = BUTTON_TAG;
        
        [infoButton addTarget:self action:@selector(showDescription:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:infoButton];
    
    }
    else{
        UIImageView *timeImageView = [[UIImageView alloc]initWithFrame: CGRectMake(100, yPos+1, 27, 27)];
        timeImageView.image = [UIImage imageNamed:@"time.png"];
        [self addSubview:timeImageView];
        
        UILabel *timeLabel = [[UILabel alloc]initWithFrame:CGRectMake(130, yPos, 130, 30)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [UIColor grayColor];
        timeLabel.text = foodDeatils.availableTime;
        [self addSubview:timeLabel];
        
        recipeImageView.image = [UIImage imageNamed:@"InfoBg.png"];
        recipeImageView.userInteractionEnabled = NO;
        
        UILabel *recipeDescription = [[UILabel alloc] initWithFrame:CGRectMake(9, 70, 300, 100)];
        recipeDescription.backgroundColor = [UIColor clearColor];
        recipeDescription.textColor = [UIColor whiteColor];
        recipeDescription.numberOfLines = 0;
        recipeDescription.textAlignment = NSTextAlignmentCenter;
        recipeDescription.text = @"Entregamos tu comida hasta las 4 pm, por favor conoce lo que habrá mañana y vuelve a partir de las 7 am para hacer tus pedidos.";
        
        [self addSubview:recipeDescription];
    }
    
}


- (void)plusTapped:(id)sender{
    
    UILabel *countLabel = (UILabel*)[self viewWithTag:ADD_TO_CART_TAG];
    
    if (countLabel){
        
        int counter = [countLabel.text intValue];
        countLabel.text = [NSString stringWithFormat:@"%d",++counter];
    }
    
    [_appDelegate addToCart:(ItemDetails *)[_appDelegate.foodArray objectAtIndex:(self.tag-1)]];
}
- (void)minusTapped:(id)sender{
    
    UILabel *countLabel = (UILabel*)[self viewWithTag:ADD_TO_CART_TAG];
    
    if (countLabel){
        
        int counter = [countLabel.text intValue];
        if (counter> 0)
            --counter;
        countLabel.text = [NSString stringWithFormat:@"%d",counter];
    }

    [_appDelegate removeFromCart:(ItemDetails *)[_appDelegate.foodArray objectAtIndex:(self.tag-1)]];
}


- (void)showDescription:(id)sender{
    
    NSLog(@"view tag: %zd", (long)self.tag);
    
    UIButton *tappedButton = (UIButton*)sender;

    
    if (!self.isInfoShowed){
        
        [tappedButton setSelected:YES];

        [self showInfoBgforTag:self.tag];
    }
    else{
        self.isInfoShowed = NO;
        [tappedButton setSelected:NO];

        [UIView animateWithDuration:0.3f animations:^{
            [self viewWithTag:656].alpha = 0.0f;
        } completion:^(BOOL finished) {
            [[self viewWithTag:656] removeFromSuperview];
    
        }];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{

    UITouch *touch = [touches anyObject];
    
    if (touch.view == recipeImageView){
        
        NSLog(@"view tag: %ldi", (long)self.tag);
        
        UIButton *tappedButton = (UIButton*)[self viewWithTag:BUTTON_TAG];
        
        if (!self.isInfoShowed){
            [tappedButton setSelected:YES];

            [self showInfoBgforTag:self.tag];
        }
        else{
            self.isInfoShowed = NO;
            [tappedButton setSelected:NO];

            [UIView animateWithDuration:0.3f animations:^{
                [self viewWithTag:656].alpha = 0.0f;
            } completion:^(BOOL finished) {
                [[self viewWithTag:656] removeFromSuperview];
            }];
        }
        
    }
}


- (void)showInfoBgforTag:(NSInteger)tag{

    [[self viewWithTag:656] removeFromSuperview];
    
    self.isInfoShowed = YES;
    
    UIImageView *infoBg = [[UIImageView alloc]initWithFrame:CGRectMake(1, 1, 318, 218)];
    infoBg.image = [UIImage imageNamed:@"InfoBg.png"];
    infoBg.tag = 656;
    infoBg.alpha = 0.0f;
    [self addSubview:infoBg];
    
    
    
    ItemDetails *foodDetails = (ItemDetails*)[_appDelegate.foodArray objectAtIndex:(self.tag-1)];
    
    
    UILabel *recipeName = [[UILabel alloc] initWithFrame:CGRectMake(9, 20, 300, 40)];
    recipeName.backgroundColor = [UIColor clearColor];
    recipeName.textColor = [UIColor whiteColor];
    recipeName.font = [UIFont boldSystemFontOfSize:20];
    recipeName.textAlignment = NSTextAlignmentCenter;
    recipeName.text = foodDetails.title;
    [infoBg addSubview:recipeName];
    
    
    
    UILabel *recipeDescription = [[UILabel alloc] initWithFrame:CGRectMake(9, 70, 300, 100)];
    recipeDescription.backgroundColor = [UIColor clearColor];
    recipeDescription.textColor = [UIColor whiteColor];
    recipeDescription.numberOfLines = 0;
    recipeDescription.textAlignment = NSTextAlignmentCenter;
    recipeDescription.text = foodDetails.description;

    [infoBg addSubview:recipeDescription];
    
    
    UILabel *recipeType = [[UILabel alloc] initWithFrame:CGRectMake(9, 180, 300, 30)];
    recipeType.backgroundColor = [UIColor clearColor];
    recipeType.textColor = [UIColor whiteColor];
    recipeType.numberOfLines = 0;
    recipeType.font = [UIFont italicSystemFontOfSize:18];
    recipeType.textAlignment = NSTextAlignmentCenter;
    recipeType.text = foodDetails.type;
    
    [infoBg addSubview:recipeType];
    
    [UIView animateWithDuration:0.3f animations:^{
        infoBg.alpha = 1.0f;

    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
