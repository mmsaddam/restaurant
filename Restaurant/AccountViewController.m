//
//  AccountViewController.m
//  Restaurant
//
//  Created by Hafizur Rahman on 6/29/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "AccountViewController.h"


#import "GTMOAuth2Authentication.h"
#import "GTMOAuth2ViewControllerTouch.h"
#import "GTMOAuth2SignIn.h"

#import <Social/Social.h>
#import <Accounts/ACAccountType.h>
#import <Accounts/ACAccountCredential.h>

#import "AppDelegate.h"

#import "SignInViewController.h"

#import "NameVC.h"

#import "PhoneNumberFormatter.h"

#import "AccountTableViewCell.h"

#define K_NAME_TAG 5588
#define K_EMAI_TAG 7755
#define K_PHONE_TAG 7733


static NSString *const kKeychainItemName = @"OAuth Sample: Google Contacts";

static NSString *const kShouldSaveInKeychainKey = @"shouldSaveInKeychain";

static NSString *const kSampleClientIDKey = @"68003974545-rr2hmc0du750el51auhch8le5a5t2pt3.apps.googleusercontent.com";
static NSString *const kSampleClientSecretKey = @"aHdXHhAxpqnwHi2obxC_TWCe";


// NSUserDefaults keys
static NSString *const kGoogleClientIDKey          = @"GoogleClientID";
static NSString *const kGoogleClientSecretKey      = @"GoogleClientSecret";

@class GTMOAuth2Authentication;

@interface AccountViewController ()
@property (nonatomic) int mNetworkActivityCounter;
@property (nonatomic, strong) GTMOAuth2Authentication *mAuth;

@property (nonatomic, retain) IBOutlet UISwitch *shouldSaveInKeychainSwitch;
@property (nonatomic, retain) GTMOAuth2Authentication *auth;
@property (nonatomic, strong) IBOutlet FBLoginView *fbLoginView;
@property (strong, nonatomic) ACAccountStore *accountStore;
@property (strong, nonatomic) ACAccount *facebookAccount;
@property (strong, nonatomic) NSDictionary *list;
@property (nonatomic, strong) IBOutlet UIButton *fbConnectButton;
@property (nonatomic, strong) IBOutlet UIButton *googleConnectButton;
@property (nonatomic, strong) IBOutlet UIButton *emailConnectionButton;
@property (nonatomic, strong) NSMutableDictionary *editDict;
@property (nonatomic, strong) PhoneNumberFormatter *formatter;


@property (nonatomic, strong) UITextField *nameFieldEdited;
@property (nonatomic, strong) UITextField *emailFieldEdited;
@property (nonatomic, strong) UITextField *phoneFieldEdited;

@property (nonatomic) BOOL nameIsEdited;
@property (nonatomic) BOOL emailIsEdited;
@property (nonatomic) BOOL phoneIsEdited;




@property (nonatomic) RegisteredService service;
@property (nonatomic, strong) IBOutlet UITableView *acDetailsTableView;
@property (nonatomic, strong) UserProfile *user;
@property (nonatomic, strong) AccountTableViewCell *cellAc;



@end

@implementation AccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - signup delegate
-(void)signUpDlegateMethod{
    NSLog(@"delegatecalled");
}

-(void)initUser:(UserProfile *)user{
    self.user = user;
}

- (void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    
  }

-(void)viewDidAppear:(BOOL)animated{
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsKey];
    self.user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    [self refreshViewController];
   
}

-(void)setUserInfo{
    NSLog(@"set sser info..");
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.formatter = [[PhoneNumberFormatter alloc]init];
    self.editDict = [[NSMutableDictionary alloc]init];
    self.nameFieldEdited = [[UITextField alloc]init];
    self.emailFieldEdited = [[UITextField alloc]init];
    self.phoneFieldEdited = [[UITextField alloc]init];
    self.nameFieldEdited.tag = NO;
    self.emailFieldEdited.tag = NO;
    self.phoneFieldEdited.tag = NO;

    
    NSLog(@"view did load..");
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed)];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Sign In" style:UIBarButtonItemStylePlain target:self action:@selector(signInButtonPressed)];
    
    //self.navigationItem.rightBarButtonItem.enabled = NO;
    
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsKey];
    self.user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    
    [self refreshViewController];


    // Do any additional setup after loading the view from its nib.
    
}

- (void)refreshViewController{

    
    if (self.user != NULL || self.user != nil){
        
        self.fbConnectButton.hidden = YES;
        self.googleConnectButton.hidden = YES;
        
        self.fbLoginView.hidden = YES;

        self.fbConnectButton.hidden = YES;
        self.emailConnectionButton.hidden = YES;
        self.acDetailsTableView.hidden = NO;
        
        [self.acDetailsTableView reloadData];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Sign Out" style:UIBarButtonItemStylePlain target:self action:@selector(signOutButtonPressed)];
        
    }else{
        
        [self showSignUpView];
    }
    
}

- (void)showSignUpView{
    
    /* Make the tableview hidden */
    self.acDetailsTableView.hidden = YES;
    
    /* Button Hidden*/
    self.googleConnectButton.hidden = NO;
    self.emailConnectionButton.hidden = NO;
    self.fbLoginView.hidden = NO;
    self.fbConnectButton.hidden = NO;
    
    NSLog(@"showSignUpView");

    
    // Check for system enabled facebook app
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]){
        
        self.fbLoginView.hidden = YES;

    }else{
        
        self.fbConnectButton.hidden = YES;

        self.fbLoginView.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    }
}


- (void)saveDataAsUserProfile:(NSDictionary*)dictionary{

    
    if ([dictionary objectForKey:@"name"] && [dictionary objectForKey:@"email"] && [dictionary objectForKey:@"gender"]){
    
        UserProfile *user = [[UserProfile alloc]init];
        
        user.name = [dictionary objectForKey:@"name"];
        user.email = [dictionary objectForKey:@"email"];
        user.gender = [dictionary objectForKey:@"gender"];
        user.phone = @"";
        user.isNewsLetter = YES;
        user.balance = @"0.0";
        user.registeredBy = self.service;
        user.creditCardNo = @"";
        user.deliveryOption = @"";
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user ];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:kUserAccountDetailsKey];
        
        self.user = user;
        
        Registration *registration = [[Registration alloc]init];
        [registration initWithUserProfile:user forCallbackSelector:@selector(registrationNotifierWithResult:) forDelegate:self];
        
        [self refreshViewController];

    }
    else{
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Registration Failed" message:@"Failed to register with this app. Please try again." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [alert show];
    }
}

- (void)registrationNotifierWithResult:(NSDictionary*)dictionary{
    
    
    if ([[dictionary objectForKey:@"result"] boolValue]){
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"singUpByEmail"];
        
    }else{
        // Delete saved data
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:kUserAccountDetailsKey];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsKey];
        self.user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];

        [self refreshViewController];
        
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Sign In" style:UIBarButtonItemStylePlain target:self action:@selector(signInButtonPressed)];


    }
    
}




-(void)resetUserProfile{
    
    self.user = nil;
    
    //self.navigationItem.rightBarButtonItem.enabled = YES;
    
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Sign In" style:UIBarButtonItemStylePlain target:self action:@selector(signInButtonPressed)];

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kUserAccountDetailsKey];
    
    [self refreshViewController];

}

- (void)signOutButtonPressed{

    switch(self.service){
        case 0: // Signup by Email
            [self signOutFromEmail];
            break;
        case 1: // Signup by Facebook
            [self signOutFromFb];
            break;
        default: // Signup by Google
            [self signOutFromGoogle];
            break;
    }
    
    
}


- (void)cancelButtonPressed{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signInButtonPressed{
    
    NSLog(@"Sign In");
    
    SignInViewController *signInViewController = [[SignInViewController alloc]initWithNibName:@"SignInViewController" bundle:[NSBundle mainBundle]];
    
    [self.navigationController pushViewController:signInViewController animated:YES];
    
}

- (IBAction)connectWithFacebook:(id)sender{

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(accountChanged:) name:ACAccountStoreDidChangeNotification object:nil];
    
    [self accessSystemFacebookUserDeatis];
}

- (IBAction)connectWithGoogle:(id)sender{
    
    [self signOutFromGoogle];
    
    [self initializeGoogle];
    
    [self callToGoogleAuth2API];
}
- (IBAction)signupWithemail:(id)sender {
   
    //SignUpEmailViewController *signVc = [[SignUpEmailViewController alloc]init];
    //[[self navigationController]pushViewController:signVc animated:YES];
    
    
    NameVC *nameVC = [[NameVC alloc]initWithNibName:@"NameVC" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:nameVC animated:YES];
}

#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{

    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    switch (section) {
        case 0:
            return 3;
            break;
        default:
            return 1;
            break;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return @"PERSONAL INFO";
            break;
        case 1:
            return @"ACCOUNT BALANCE";
            break;
        default:
            return @"CREDIT CARD";
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *const CellIdentifier = @"CellIdentifier";
    
    [self.user showUserProfile];
    
    

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    cell.detailTextLabel.textColor = [UIColor redColor];
    
    if (indexPath.section == 2) {
        cell.textLabel.text = @"N/A";
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    if(indexPath.section == 1){
        cell.textLabel.text = @"credits";
        cell.detailTextLabel.text = self.user.balance;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    if (indexPath.section == 0) {
        static NSString *const customCellIdentifier = @"CustomCellIdentifier";

         self.cellAc = [tableView dequeueReusableCellWithIdentifier:customCellIdentifier];
        if (!self.cellAc){
            
            self.cellAc = [[AccountTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"AccountTableViewCell" owner:self options:nil];
            self.cellAc = [nib objectAtIndex:0];
           self.cellAc.itemValueField.delegate = self;
            
            
        }

        switch (indexPath.row) {
            case 0:
                self.cellAc.itemTitleLabel.text = @"Name";
                self.cellAc.itemValueField.text = self.user.name;
                self.cellAc.itemValueField.tag = indexPath.row;
                self.cellAc.itemValueField.returnKeyType = UIReturnKeyNext;
                self.cellAc.itemValueField.keyboardType = UIKeyboardTypeDefault;

                [ self.cellAc.itemValueField addTarget:self
                              action:@selector(textFieldDidChange:)
                    forControlEvents:UIControlEventEditingChanged];
                break;
            case 1:
                self.cellAc.itemTitleLabel.text = @"Email";
                self.cellAc.itemValueField.text = self.user.email;
                self.cellAc.itemValueField.tag = indexPath.row;
                self.cellAc.itemValueField.returnKeyType = UIReturnKeyNext;
                self.cellAc.itemValueField.keyboardType = UIKeyboardTypeEmailAddress;

                [ self.cellAc.itemValueField addTarget:self
                                                action:@selector(textFieldDidChange:)
                                      forControlEvents:UIControlEventEditingChanged];

                break;
            case 2:
            {
                self.cellAc.itemTitleLabel.text = @"Phone";
                self.cellAc.itemValueField.tag = indexPath.row;
                self.cellAc.itemValueField.returnKeyType = UIReturnKeyNext;
                self.cellAc.itemValueField.keyboardType = UIKeyboardTypeNumberPad;
                self.cellAc.itemValueField.text = self.user.phone;
                [ self.cellAc.itemValueField addTarget:self
                                                action:@selector(textFieldDidChange:)
                                      forControlEvents:UIControlEventEditingChanged];

            }
                break;
                
            default:
                break;
        }
         return  self.cellAc;
    }
    
    
       return cell;
    
    
}
- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
}

#pragma mark - textfield delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self saveButttonPressedAfterEdit];
    return YES;
}


-(void)textFieldDidChange:(UITextField *)textField{
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(saveButttonPressedAfterEdit)];

    switch (textField.tag) {
        case 0:
            self.nameFieldEdited = textField;
            self.nameIsEdited = YES;
            break;
        case 1:
            self.emailFieldEdited = textField;
            self.emailIsEdited = YES;
            break;
        case 2:
            textField.text = [self.formatter format:textField.text withLocale:@"us"];
            self.phoneFieldEdited = textField;
            self.phoneIsEdited = YES;

            break;
            
        default:
            break;
    }
   
}



-(void)saveButttonPressedAfterEdit{
    
    
    if (self.nameIsEdited) {
        if ([self nameValidation:self.nameFieldEdited]) {
            self.user.name = self.nameFieldEdited.text;
            self.nameIsEdited = NO;
        }
        else{
            [self.nameFieldEdited becomeFirstResponder];
            return;
        }
        
        
    }
    if (self.emailIsEdited) {
        if ([self emailValidation: self.emailFieldEdited]) {
            self.user.email = self.emailFieldEdited.text;
            self.emailIsEdited = NO;

        }
        else{
            [self.emailFieldEdited becomeFirstResponder];

            return;
        }
    }
    if (self.phoneIsEdited) {
        if ([self phoneValidation:self.phoneFieldEdited]) {
            self.user.phone = self.phoneFieldEdited.text;
            self.phoneFieldEdited = NO;
        }
        else{
            [self.phoneFieldEdited becomeFirstResponder];

            return;
        }
        
    }

    self.phoneFieldEdited.text = nil;
    self.emailFieldEdited.text = nil;
    self.nameFieldEdited.text = nil;

    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.user ];

    [[NSUserDefaults standardUserDefaults]setObject:data forKey:kUserAccountDetailsKey];
    
    [self refreshViewController];



}

#pragma mark - Input Validation

-(BOOL)nameValidation:(UITextField *)textField{
    BOOL ret = YES;
    if ([textField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Name field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
       ret = NO;
    }
    return ret;
}

-(BOOL)emailValidation:(UITextField *)textField{
    
    BOOL ret = YES;
    
    if ([textField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email address field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
      ret = NO;
    }
    else
    if (![self stringIsValidEmail:textField.text]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email address is not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        ret = NO;
    }
    else{
        ret =  YES;
    }
    
    return ret;
}

- (BOOL) stringIsValidEmail:(NSString *)checkString
{
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(BOOL)phoneValidation:(UITextField *)textField{
    BOOL ret = YES;
    if ([textField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Phone Number field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        ret = NO;
    }
    else
    if ([textField.text length]< 10){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Phone Number is not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        ret = NO;
    }
    return  ret;
}



- (void)switchValueChanged:(id)sender{

    UISwitch *sw = (UISwitch*)sender;
    
    self.user.isNewsLetter = sw.isOn;
    
    // Save data
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:self.user ];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kUserAccountDetailsKey];
    
}


#pragma mark - Google Service

- (void)callToGoogleAuth2API{
    
    NSString *keychainItemName = nil;
    if ([self shouldSaveInKeychain]) {
        keychainItemName = kKeychainItemName;
    }
    
    // For Google APIs, the scope strings are available
    // in the service constant header files.
    NSString *scope = @"https://www.googleapis.com/auth/userinfo.profile";
    
    // Typically, applications will hardcode the client ID and client secret
    // strings into the source code; they should not be user-editable or visible.
    //
    // But for this sample code, they are editable.
    NSString *clientID = kSampleClientIDKey;
    NSString *clientSecret = kSampleClientSecretKey;
    
    if ([clientID length] == 0 || [clientSecret length] == 0) {
        NSString *msg = @"The sample code requires a valid client ID and client secret to sign in.";
        [self displayAlertWithMessage:msg];
        return;
    }
    
    // Note:
    // GTMOAuth2ViewControllerTouch is not designed to be reused. Make a new
    // one each time you are going to show it.
    
    // Display the autentication view.
    SEL finishedSel = @selector(viewController:finishedWithAuth:error:);
    
    GTMOAuth2ViewControllerTouch *viewController;
    viewController = [GTMOAuth2ViewControllerTouch controllerWithScope:scope
                                                              clientID:clientID
                                                          clientSecret:clientSecret
                                                      keychainItemName:keychainItemName
                                                              delegate:self
                                                      finishedSelector:finishedSel];
    
    // You can set the title of the navigationItem of the controller here, if you
    // want.
    
    // If the keychainItemName is not nil, the user's authorization information
    // will be saved to the keychain. By default, it saves with accessibility
    // kSecAttrAccessibleAfterFirstUnlockThisDeviceOnly, but that may be
    // customized here. For example,
    //
    //   viewController.keychainItemAccessibility = kSecAttrAccessibleAlways;
    
    // During display of the sign-in window, loss and regain of network
    // connectivity will be reported with the notifications
    // kGTMOAuth2NetworkLost/kGTMOAuth2NetworkFound
    //
    // See the method signInNetworkLostOrFound: for an example of handling
    // the notification.
    
    // Optional: Google servers allow specification of the sign-in display
    // language as an additional "hl" parameter to the authorization URL,
    // using BCP 47 language codes.
    //
    // For this sample, we'll force English as the display language.
    NSDictionary *params = [NSDictionary dictionaryWithObject:@"en"
                                                       forKey:@"hl"];
    viewController.signIn.additionalAuthorizationParameters = params;
    
    // By default, the controller will fetch the user's email, but not the rest of
    // the user's profile.  The full profile can be requested from Google's server
    // by setting this property before sign-in:
    //
    viewController.signIn.shouldFetchGoogleUserProfile = YES;
    //
    // The profile will be available after sign-in as
    //
    
    // Optional: display some html briefly before the sign-in page loads
    NSString *html = @"<html><body bgcolor=silver><div align=center>Loading sign-in page...</div></body></html>";
    viewController.initialHTMLString = html;
    
    [[self navigationController] pushViewController:viewController animated:YES];
    
    // The view controller will be popped before signing in has completed, as
    // there are some additional fetches done by the sign-in controller.
    // The kGTMOAuth2UserSignedIn notification will be posted to indicate
    // that the view has been popped and those additional fetches have begun.
    // It may be useful to display a temporary UI when kGTMOAuth2UserSignedIn is
    // posted, just until the finished selector is invoked.

}

- (void)initializeGoogle{

    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(incrementNetworkActivity:) name:kGTMOAuth2WebViewStartedLoading object:nil];
    [nc addObserver:self selector:@selector(decrementNetworkActivity:) name:kGTMOAuth2WebViewStoppedLoading object:nil];
    [nc addObserver:self selector:@selector(incrementNetworkActivity:) name:kGTMOAuth2FetchStarted object:nil];
    [nc addObserver:self selector:@selector(decrementNetworkActivity:) name:kGTMOAuth2FetchStopped object:nil];
    [nc addObserver:self selector:@selector(signInNetworkLostOrFound:) name:kGTMOAuth2NetworkLost  object:nil];
    [nc addObserver:self selector:@selector(signInNetworkLostOrFound:) name:kGTMOAuth2NetworkFound object:nil];
    
    // Fill in the Client ID and Client Secret text fields
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    // First, we'll try to get the saved Google authentication, if any, from
    // the keychain
    
    // Normal applications will hardcode in their client ID and client secret,
    // but the sample app allows the user to enter them in a text field, and
    // saves them in the preferences
    NSString *clientID = [defaults stringForKey:kGoogleClientIDKey];
    NSString *clientSecret = [defaults stringForKey:kGoogleClientSecretKey];
    
    GTMOAuth2Authentication *auth = nil;
    
    if (clientID && clientSecret) {
        auth = [GTMOAuth2ViewControllerTouch authForGoogleFromKeychainForName:kKeychainItemName
                                                                     clientID:clientID
                                                                 clientSecret:clientSecret];
    }
    
    
    // Save the authentication object, which holds the auth tokens and
    // the scope string used to obtain the token.  For Google services,
    // the auth object also holds the user's email address.
    self.auth = auth;
    
    // Update the client ID value text fields to match the radio button selection
    [self loadClientIDValues];
    
    BOOL isRemembering = [self shouldSaveInKeychain];
    self.shouldSaveInKeychainSwitch.on = isRemembering;
    [self updateUI];
}

- (BOOL)isSignedIn {
    BOOL isSignedIn = self.auth.canAuthorize;
    return isSignedIn;
}
-(void)signOutFromFb{
    
    NSLog(@"signOutFromFb");
    if (FBSession.activeSession.state == FBSessionStateOpen
        || FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
        [FBSession.activeSession closeAndClearTokenInformation];
    }

    
    [self resetUserProfile];
}


-(void)signOutFromEmail{
    NSLog(@"signOutFromEmail");
   // [];
    
    [self resetUserProfile];

}


- (void)signOutFromGoogle {
    if ([self.auth.serviceProvider isEqual:kGTMOAuth2ServiceProviderGoogle]) {
        // remove the token from Google's servers
        [GTMOAuth2ViewControllerTouch revokeTokenForGoogleAuthentication:self.auth];
    }
    
    // remove the stored Google authentication from the keychain, if any
    [GTMOAuth2ViewControllerTouch removeAuthFromKeychainForName:kKeychainItemName];
    
    // Discard our retained authentication object.
    self.auth = nil;
    
    [self updateUI];
    
    [self resetUserProfile];

}

- (void)viewController:(GTMOAuth2ViewControllerTouch *)viewController
      finishedWithAuth:(GTMOAuth2Authentication *)auth
                 error:(NSError *)error {
    if (error != nil) {
        // Authentication failed (perhaps the user denied access, or closed the
        // window before granting access)
        NSLog(@"Authentication error: %@", error);
        NSData *responseData = [[error userInfo] objectForKey:@"data"]; // kGTMHTTPFetcherStatusDataKey
        if ([responseData length] > 0) {
            // show the body of the server's authentication failure response
            NSString *str = [[NSString alloc] initWithData:responseData
                                                   encoding:NSUTF8StringEncoding];
            NSLog(@"%@", str);
        }
        
        self.auth = nil;
    } else {
        // Authentication succeeded
        //
        // At this point, we either use the authentication object to explicitly
        // authorize requests, like
        //
        //  [auth authorizeRequest:myNSURLMutableRequest
        //       completionHandler:^(NSError *error) {
        //         if (error == nil) {
        //           // request here has been authorized
        //         }
        //       }];
        //
        // or store the authentication object into a fetcher or a Google API service
        // object like
        //
        //   [fetcher setAuthorizer:auth];
        
        // save the authentication object
        self.auth = auth;
        
        NSDictionary *profile = viewController.signIn.userProfile;
        
        NSLog(@"Profile: %@", profile);
        
        self.service = Google;

        [self performSelectorOnMainThread:@selector(saveDataAsUserProfile:) withObject:profile waitUntilDone:NO];

    }
    
    [self updateUI];
}

- (void)incrementNetworkActivity:(NSNotification *)notify {
    ++self.mNetworkActivityCounter;
    if (self.mNetworkActivityCounter == 1) {
        UIApplication *app = [UIApplication sharedApplication];
        [app setNetworkActivityIndicatorVisible:YES];
    }
}

- (void)decrementNetworkActivity:(NSNotification *)notify {
    --self.mNetworkActivityCounter;
    if (self.mNetworkActivityCounter == 0) {
        UIApplication *app = [UIApplication sharedApplication];
        [app setNetworkActivityIndicatorVisible:NO];
    }
}

- (void)signInNetworkLostOrFound:(NSNotification *)notify {
    if ([[notify name] isEqual:kGTMOAuth2NetworkLost]) {
        // network connection was lost; alert the user, or dismiss
        // the sign-in view with
        //   [[[notify object] delegate] cancelSigningIn];
    } else {
        // network connection was found again
    }
}


- (void)updateUI {
    // update the text showing the signed-in state and the button title
    // A real program would use NSLocalizedString() for strings shown to the user.
    if ([self isSignedIn]) {
        // signed in
        //self.serviceNameField.text = self.auth.serviceProvider;
        //self.emailField.text = self.auth.userEmail;
        //self.accessTokenField.text = self.auth.accessToken;
        //self.expirationField.text = [self.auth.expirationDate description];
        //self.refreshTokenField.text = self.auth.refreshToken;
        
        //self.signInOutButton.title = @"Sign Out";
        
        NSLog(@"Provider: %@", self.auth.serviceProvider);
        NSLog(@"Email: %@", self.auth.userEmail);
        NSLog(@"Access Token: %@", self.auth.accessToken);
        NSLog(@"Expiration: %@", [self.auth.expirationDate description]);
        NSLog(@"Refresh Token: %@", self.auth.refreshToken);
        
        //NSLog(@"First Name: %@", self.auth.)
        

    } else {
        // signed out

    }
    
    BOOL isRemembering = [self shouldSaveInKeychain];
    self.shouldSaveInKeychainSwitch.on = isRemembering;
}

- (BOOL)shouldSaveInKeychain {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL flag = [defaults boolForKey:kShouldSaveInKeychainKey];
    return flag;
}

- (void)saveClientIDValues {
    // Save the client ID and secret from the text fields into the prefs
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *clientID = kSampleClientIDKey;
    NSString *clientSecret = kSampleClientSecretKey;
    
    [defaults setObject:clientID forKey:kGoogleClientIDKey];
    [defaults setObject:clientSecret forKey:kGoogleClientSecretKey];
}

- (void)loadClientIDValues {
    // Load the client ID and secret from the prefs into the text fields
    //NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    //self.clientIDField.text = [defaults stringForKey:kGoogleClientIDKey];
    //self.clientSecretField.text = [defaults stringForKey:kGoogleClientSecretKey];
}

- (void)displayAlertWithMessage:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OAuth2Sample"
                                                     message:message
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
    [alert show];
}

#pragma mark - FBLoginView Delegate

// This method will be called when the user information has been fetched
- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    //self.profilePictureView.profileID = user.id;
    //self.nameLabel.text = user.name;
    
    
    
    NSDictionary *dictionary = (NSDictionary*)user;
    
    
    NSLog(@"user.name: %@", user.name);
    NSLog(@"user.name: %@", user.birthday);
    
    NSLog(@"User: %@", dictionary);
    
    NSLog(@"Name: %@\nEmail: %@\nGender: %@", [dictionary objectForKey:@"name"], [dictionary objectForKey:@"email"], [dictionary objectForKey:@"gender"]);
    
    self.service = Facebook;
    
    [self performSelectorOnMainThread:@selector(saveDataAsUserProfile:) withObject:dictionary waitUntilDone:NO];

}

// Logged-in user experience
- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView {
    //self.statusLabel.text = @"You're logged in as";
}

// Logged-out user experience
- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView {
    //self.profilePictureView.profileID = nil;
    //self.nameLabel.text = @"";
    //self.statusLabel.text= @"You're not logged in!";
}

- (void)accessSystemFacebookUserDeatis{
    
    self.accountStore = [[ACAccountStore alloc]init];
    ACAccountType *FBaccountType= [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    NSString *key = @"326083627479167";
    NSDictionary *dictFB = [NSDictionary dictionaryWithObjectsAndKeys:key,ACFacebookAppIdKey,@[@"email"],ACFacebookPermissionsKey, nil];
    
    
    [self.accountStore requestAccessToAccountsWithType:FBaccountType options:dictFB completion:
     ^(BOOL granted, NSError *e) {
         if (granted) {
             NSArray *accounts = [self.accountStore accountsWithAccountType:FBaccountType];
             //it will always be the last object with single sign on
             self.facebookAccount = [accounts lastObject];
             NSLog(@"facebook account =%@",self.facebookAccount);
             [self get];
         } else {
             //Fail gracefully...
             NSLog(@"error getting permission %@",e);
             
         }
     }];
}

-(void)get
{
    
    NSURL *requestURL = [NSURL URLWithString:@"https://graph.facebook.com/me"];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                            requestMethod:SLRequestMethodGET
                                                      URL:requestURL
                                               parameters:nil];
    request.account = self.facebookAccount;
    
    [request performRequestWithHandler:^(NSData *data,
                                         NSHTTPURLResponse *response,
                                         NSError *error) {
        
        if(!error)
        {
            self.list =[NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
            
            NSLog(@"Dictionary contains: %@", self.list );
            
            self.service = Facebook;

            [self performSelectorOnMainThread:@selector(saveDataAsUserProfile:) withObject:self.list waitUntilDone:NO];
            
            if([self.list objectForKey:@"error"]!=nil)
            {
                [self attemptRenewCredentials];
            }
            dispatch_async(dispatch_get_main_queue(),^{
                //nameLabel.text = [list objectForKey:@"username"];
            });
        }
        else{
            //handle error gracefully
            NSLog(@"error from get%@",error);
            //attempt to revalidate credentials
        }
        
    }];
    
}

-(void)accountChanged:(NSNotification *)notif//no user info associated with this notif
{
    [self attemptRenewCredentials];
}
-(void)attemptRenewCredentials{
    [self.accountStore renewCredentialsForAccount:(ACAccount *)self.facebookAccount completion:^(ACAccountCredentialRenewResult renewResult, NSError *error){
        if(!error)
        {
            switch (renewResult) {
                case ACAccountCredentialRenewResultRenewed:
                    NSLog(@"Good to go");
                    [self get];
                    break;
                case ACAccountCredentialRenewResultRejected:
                    NSLog(@"User declined permission");
                    break;
                case ACAccountCredentialRenewResultFailed:
                    NSLog(@"non-user-initiated cancel, you may attempt to retry");
                    break;
                default:
                    break;
            }
        }
        else{
            //handle error gracefully
            NSLog(@"error from renew credentials%@",error);
        }
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
