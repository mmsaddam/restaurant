//
//  UserProfile.h
//  Restaurant
//
//  Created by Hafizur Rahman on 6/29/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    
    Email = 0,
    Facebook,
    Google
    
} RegisteredService;

static NSString *const kUserAccountDetailsKey = @"User_Profile";
static NSString *const kUserAccountDetailsTempKey = @"User_Profile_Temp";


@interface UserProfile : NSObject<NSCoding>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *location;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *creditCardNo;
@property (nonatomic, strong) NSString *balance;
@property (nonatomic) RegisteredService registeredBy;
@property (nonatomic) BOOL isNewsLetter;
@property (nonatomic, strong) NSString *deliveryOption;

- (void)showUserProfile;

@end
