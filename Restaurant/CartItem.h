//
//  OrderDetails.h
//  Restaurant
//
//  Created by Hafizur Rahman on 7/6/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemDetails.h"

@interface CartItem : NSObject

@property (nonatomic, strong) ItemDetails *foodItem;
@property (nonatomic) NSInteger counter;
@end
