//
//  RecipeView.h
//  Restaurant
//
//  Created by Hafizur Rahman on 7/4/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemDetails.h"

@interface RecipeView : UIView


- (void)addFoodMenu:(ItemDetails*)foodDeatils;
@end
