//
//  OrderCell.h
//  Restaurant
//
//  Created by Hafizur Rahman on 7/6/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderCell : UITableViewCell


@property (nonatomic, strong) IBOutlet UILabel *firstNumberOfItemLablel;
@property (nonatomic, strong) IBOutlet UILabel *firstItemTitleLablel;

@property (nonatomic, strong) IBOutlet UILabel *firstItemPriceLablel;

@property (nonatomic, strong) IBOutlet UILabel *secondNumberOfItemLablel;
@property (nonatomic, strong) IBOutlet UILabel *secondItemTitleLablel;

@property (nonatomic, strong) IBOutlet UILabel *secondItemPriceLablel;

@property (nonatomic, strong) IBOutlet UILabel *thirdNumberOfItemLablel;
@property (nonatomic, strong) IBOutlet UILabel *thirdItemTitleLablel;

@property (nonatomic, strong) IBOutlet UILabel *thirdItemPriceLablel;

@end
