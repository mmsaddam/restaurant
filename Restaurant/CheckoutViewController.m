//
//  CheckoutViewController.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/6/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "CheckoutViewController.h"
#import "OrderCell.h"
#import "AppDelegate.h"
#import "PhoneNumberFormatter.h"

#define TIP_TEXT_FIELD_TAG 543

@interface CheckoutViewController ()

@property (nonatomic, strong) IBOutlet UITableView *detailsTableView;
@property (nonatomic, strong) AppDelegate *appDelegate;
@property (nonatomic, strong) UserProfile *user;
@property (nonatomic) NSInteger totalAmountToPaid;
@end

@implementation CheckoutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Checkout";
    
    _appDelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsKey];
    self.user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    [self.user showUserProfile];
    
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Confirm" style:UIBarButtonItemStylePlain target:self action:@selector(confirmPurchase)];
//    self.navigationItem.rightBarButtonItem.tintColor = [UIColor redColor];

}

- (void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
    
    
}

- (void)keyboardWillShow:(NSNotification*)noti{

    
}

- (void)keyboardWillHide:(NSNotification*)noti{
    
}

- (void)confirmPurchase{

    NSLog(@"confirm pressed..");
}


#pragma mark - UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 4 && indexPath.row == 0){
    
        if (_appDelegate.cartArray.count == 1) return 24;
        if (_appDelegate.cartArray.count == 2) return 47;
        if (_appDelegate.cartArray.count == 3) return 68;

    }
    
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    
    if (section == 4) {
        return 96;
    }
    else
        return 0;
    
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    
    if (section == 4) {
        CGRect footerFrame = [tableView rectForFooterInSection:4];
        
        UIButton *confirmButton = [[UIButton alloc]initWithFrame:CGRectMake(10, 44, footerFrame.size.width-20, 44)];
        
        [confirmButton setTitle:@"Confirm" forState:UIControlStateNormal];
        confirmButton.titleLabel.textColor = [UIColor whiteColor];
        confirmButton.backgroundColor = [UIColor redColor];
        [confirmButton addTarget:self action:@selector(confirmPurchase) forControlEvents:UIControlEventTouchUpInside];
        UIView *footerView = [[UIView alloc]initWithFrame:footerFrame];
        [footerView addSubview:confirmButton];
        
        return footerView;
    }
    else return nil;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    switch (section) {
        case 0:
        case 1:
        case 2:
        case 3:
            return 1;
            break;
        default:
            return 2;
            break;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    switch (section) {
        case 0:
            return @"ADDRESS";
            break;
            
        case 1:
            return @"PHONE";
            break;
            
        case 2:
            return @"PAID THROUGH";
            break;
            
        case 3:
            return @"DELIVERY";
            break;
            
        default:
            return @"ORDER DETAILS";
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *const CellIdentifier = @"CellIdentifier";
    
    // Normal Cell

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%zd-%@-%zd", (long)indexPath.section, CellIdentifier, (long)indexPath.row]];
    
    if (!cell){
        
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    OrderCell *orderCell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"%zd-%@-%zd", (long)indexPath.section, CellIdentifier, (long)indexPath.row]];
    
    if (!orderCell){
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"OrderCell" owner:self options:nil];
        for (id currentObject in topLevelObjects){
            if ([currentObject isKindOfClass:[UITableViewCell class]]){
                orderCell =  (OrderCell *) currentObject;
                break;
            }
        }
    }

    
    if (indexPath.section == 0){
        cell.textLabel.text = self.user.location;
    }
    else if (indexPath.section == 1){
        
        PhoneNumberFormatter *formatter = [[PhoneNumberFormatter alloc]init];
        cell.textLabel.text = [formatter format:self.user.phone withLocale:@"es_MX"];
    }
    else if (indexPath.section == 2){
        cell.textLabel.text = @"Cash on Delivery";
        orderCell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if (indexPath.section == 3){
        cell.textLabel.text = self.user.deliveryOption;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    else if (indexPath.section == 4 && indexPath.row ==0){
        
        if (_appDelegate.cartArray.count == 1){
        
            CartItem *cartItem = (CartItem*)[_appDelegate.cartArray objectAtIndex:0];
            ItemDetails *details = cartItem.foodItem;
            orderCell.firstNumberOfItemLablel.text = [NSString stringWithFormat:@"%zd x", (long)cartItem.counter];
            orderCell.firstItemTitleLablel.text = details.title;
            orderCell.firstItemPriceLablel.text = [NSString stringWithFormat:@"$%zd", cartItem.counter*([details.price intValue])];
            _totalAmountToPaid = cartItem.counter*([details.price intValue]);
        }
        
        else if (_appDelegate.cartArray.count == 2){
            
            CartItem *cartItem1 = (CartItem*)[_appDelegate.cartArray objectAtIndex:1];
            ItemDetails *details1 = cartItem1.foodItem;
            orderCell.secondNumberOfItemLablel.text = [NSString stringWithFormat:@"%zd x", (long)cartItem1.counter];
            orderCell.secondItemTitleLablel.text = details1.title;
            orderCell.secondItemPriceLablel.text = [NSString stringWithFormat:@"$%zd", cartItem1.counter*([details1.price intValue])];
            
            _totalAmountToPaid += cartItem1.counter*([details1.price intValue]);
            
            CartItem *cartItem = (CartItem*)[_appDelegate.cartArray objectAtIndex:0];
            ItemDetails *details = cartItem.foodItem;
            orderCell.firstNumberOfItemLablel.text = [NSString stringWithFormat:@"%zd x", (long)cartItem.counter];
            orderCell.firstItemTitleLablel.text = details.title;
            orderCell.firstItemPriceLablel.text = [NSString stringWithFormat:@"$%zd", cartItem.counter*([details.price intValue])];
            
            _totalAmountToPaid += cartItem.counter*([details.price intValue]);

        }
        
        else if (_appDelegate.cartArray.count == 3){
            
            CartItem *cartItem2 = (CartItem*)[_appDelegate.cartArray objectAtIndex:2];
            ItemDetails *details2 = cartItem2.foodItem;
            orderCell.thirdNumberOfItemLablel.text = [NSString stringWithFormat:@"%zd x", (long)cartItem2.counter];
            orderCell.thirdItemTitleLablel.text = details2.title;
            orderCell.thirdItemPriceLablel.text = [NSString stringWithFormat:@"$%zd", cartItem2.counter*([details2.price intValue])];
            
            _totalAmountToPaid += cartItem2.counter*([details2.price intValue]);

            
            CartItem *cartItem1 = (CartItem*)[_appDelegate.cartArray objectAtIndex:1];
            ItemDetails *details1 = cartItem1.foodItem;
            orderCell.secondNumberOfItemLablel.text = [NSString stringWithFormat:@"%zd x", (long)cartItem1.counter];
            orderCell.secondItemTitleLablel.text = details1.title;
            orderCell.secondItemPriceLablel.text = [NSString stringWithFormat:@"$%zd", cartItem1.counter*([details1.price intValue])];
            
            _totalAmountToPaid += cartItem1.counter*([details1.price intValue]);

            
            CartItem *cartItem = (CartItem*)[_appDelegate.cartArray objectAtIndex:0];
            ItemDetails *details = cartItem.foodItem;
            orderCell.firstNumberOfItemLablel.text = [NSString stringWithFormat:@"%zd x", (long)cartItem.counter];
            orderCell.firstItemTitleLablel.text = details.title;
            orderCell.firstItemPriceLablel.text = [NSString stringWithFormat:@"$%zd", cartItem.counter*([details.price intValue])];
            
            _totalAmountToPaid += cartItem.counter*([details.price intValue]);

        }
        
        orderCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return orderCell;
    }/*
    else if (indexPath.section == 3 && indexPath.row == 1){
        cell.textLabel.text = @"Tax";
        cell.detailTextLabel.text = @"$0.00";
        cell.detailTextLabel.textAlignment = NSTextAlignmentRight;
        cell.detailTextLabel.textColor = [UIColor redColor];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:12];
        
        _totalAmountToPaid += 0;


    }
    else if (indexPath.section == 3 && indexPath.row == 2){
    
        cell.textLabel.text = @"Tip";
        
        UITextField *addTipField = [[UITextField alloc]initWithFrame:CGRectMake(200, 12, 100, 20)];
        addTipField.borderStyle = UITextBorderStyleNone;
        addTipField.placeholder = @"Add Tip";
        addTipField.delegate = self;
        addTipField.tag = TIP_TEXT_FIELD_TAG;
        addTipField.textAlignment = NSTextAlignmentRight;
        addTipField.font = [UIFont systemFontOfSize:12];
        cell.accessoryView = addTipField;
        
        _totalAmountToPaid += [addTipField.text intValue];
    }*/
    else if (indexPath.section == 4 && indexPath.row == 1){
    
        cell.textLabel.text = @"Total";
        cell.textLabel.font = [UIFont boldSystemFontOfSize:20];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"$%zd",(long)_totalAmountToPaid];
        cell.detailTextLabel.textColor = [UIColor redColor];
        cell.detailTextLabel.textAlignment = NSTextAlignmentRight;
        cell.detailTextLabel.font = [UIFont boldSystemFontOfSize:20];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

    }

    

    return cell;

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section == 0){
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Location" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
        alert.tag = 901;
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert show];
    }
    else if (indexPath.section == 1){
    
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"New Phone Number" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Save", nil];
        alert.tag = 902;
        [alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert textFieldAtIndex:0].keyboardType = UIKeyboardTypePhonePad;

        [alert show];
    }
    
    else if (indexPath.section == 3){
    
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Escoge la hora de entrega" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"Entrega inmediata",@"1:30 - 2:00 PM",@"2:00 - 2:30 PM",@"2:30 - 3:00 PM",@"3:00 - 3:30 PM",@"3:30 - 4:00 PM",@"4:00 - 4:30 PM", nil];
        [actionSheet showInView:self.view];
    }
}

#pragma mark - UIAlertView Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == 901){
    
        if (buttonIndex == 1 && [[alertView textFieldAtIndex:0].text length]> 0){
            
            self.user.location = [alertView textFieldAtIndex:0].text;
        }
        
    }
    
    if (alertView.tag == 902){
    
        if (buttonIndex == 1 && [[alertView textFieldAtIndex:0].text length]> 0){
            
            self.user.phone = [alertView textFieldAtIndex:0].text;
        }
    }
    
    [self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];
    
    
}

#pragma mark - UIActionsheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{

    
    NSLog(@"Selected: %@", [actionSheet buttonTitleAtIndex:buttonIndex]);
    
    self.user.deliveryOption = [actionSheet buttonTitleAtIndex:buttonIndex];

    
    [self performSelectorOnMainThread:@selector(reloadTable) withObject:nil waitUntilDone:NO];

}

- (void)reloadTable{
    
    NSData *newData = [NSKeyedArchiver archivedDataWithRootObject:self.user];
    [[NSUserDefaults standardUserDefaults] setObject:newData forKey:kUserAccountDetailsKey];
    
    _totalAmountToPaid = 0;
    
    [self.detailsTableView reloadData];

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
