//
//  NameVC.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "NameVC.h"
#import "AppDelegate.h"
#import "EmailVC.h"

@interface NameVC ()

@property (nonatomic, strong) IBOutlet UITextField *nameTextField;
@end

@implementation NameVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Name";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(submitSignInDetailsPressed)];
    
    [self.nameTextField becomeFirstResponder];
}


- (void)submitSignInDetailsPressed{
    
    if ([self.nameTextField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Name field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    
    UserProfile *user = [[UserProfile alloc]init];
    
    user.name = self.nameTextField.text;
    user.email = @"";
    user.gender = @"";
    user.phone = @"";
    user.password = @"";
    user.isNewsLetter = YES;
    user.balance = @"0.0";
    user.registeredBy = Email;
    user.creditCardNo = @"";
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user ];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:kUserAccountDetailsTempKey];
    
    EmailVC *nameVC = [[EmailVC alloc]initWithNibName:@"EmailVC" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:nameVC animated:YES];
    
    
}


#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self submitSignInDetailsPressed];

    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
