//
//  ItemDetails.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/6/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "ItemDetails.h"

@implementation ItemDetails

@synthesize title, image, description, price, type, itemNo;
@synthesize availableTime;


- (void)showDetails{
    
    NSLog(@"Item: \n%@\n%@\n%@\n%@\n%@\n%ldi\n%@",title,image,description,price,type,(long)itemNo,availableTime);
}
@end
