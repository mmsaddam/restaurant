//
//  GenderVC.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "GenderVC.h"
#import "AppDelegate.h"

@interface GenderVC ()

@property (nonatomic, strong) IBOutlet UITextField *genderField;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *indicatorView;
@end

@implementation GenderVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Gender";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStylePlain target:self action:@selector(submitSignInDetailsPressed)];

    [self.genderField becomeFirstResponder];

}



- (void)submitSignInDetailsPressed{
    
    if ([self.genderField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Phone Number field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsTempKey];
    UserProfile *user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    user.gender = self.genderField.text;
    
    NSData *newData = [NSKeyedArchiver archivedDataWithRootObject:user ];
    [[NSUserDefaults standardUserDefaults] setObject:newData forKey:kUserAccountDetailsKey];
    
    
    [self.indicatorView startAnimating];
    
    Registration *registration = [[Registration alloc]init];
    [registration initWithUserProfile:user forCallbackSelector:@selector(registrationNotifierWithResult:) forDelegate:self];
    // Save user details data to device
}

- (void)registrationNotifierWithResult:(NSDictionary*)dictionary{
    
    
    if ([[dictionary objectForKey:@"result"] boolValue]){
        
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"singUpByEmail"];
        
        [[self navigationController] popToRootViewControllerAnimated:YES];
    }else{
        // Delete save data
        [[NSUserDefaults standardUserDefaults]removeObjectForKey:kUserAccountDetailsKey];
    }
    
    [self.indicatorView stopAnimating];

}


#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self submitSignInDetailsPressed];
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
