//
//  AccountTableViewCell.h
//  Restaurant
//
//  Created by Muzahidul on 7/6/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UILabel *itemTitleLabel;
@property (nonatomic, strong) IBOutlet UITextField *itemValueField;
@end
