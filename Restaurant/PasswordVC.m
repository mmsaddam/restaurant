//
//  PasswordVC.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "PasswordVC.h"
#import "AppDelegate.h"
#import "GenderVC.h"

@interface PasswordVC ()

@property (nonatomic, strong) IBOutlet UITextField *passwordTextField;
@property (nonatomic, strong) IBOutlet UITextField *passwordConfirmField;
@end

@implementation PasswordVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Password";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(submitSignInDetailsPressed)];
    
    [self.passwordTextField becomeFirstResponder];

}


- (void)submitSignInDetailsPressed{
    
    
    if ([self.passwordTextField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Password field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (![self.passwordTextField.text isEqualToString:self.passwordConfirmField.text]){ // Checking matching
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Password does not matched" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsTempKey];
    UserProfile *user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    user.password = self.passwordTextField.text;
    
    NSData *newData = [NSKeyedArchiver archivedDataWithRootObject:user ];
    [[NSUserDefaults standardUserDefaults] setObject:newData forKey:kUserAccountDetailsTempKey];
    
    GenderVC *nameVC = [[GenderVC alloc]initWithNibName:@"GenderVC" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:nameVC animated:YES];
}


#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    
    if (textField.tag == 100)
        [self.passwordConfirmField becomeFirstResponder];
    else if (textField.tag == 101){
        
        [self submitSignInDetailsPressed];
    }

    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
