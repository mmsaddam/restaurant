//
//  TitleView.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/5/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "TitleView.h"

@implementation TitleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)titleWithImage:(UIImage*)titleImage withDescription:(NSString*)description{
    
    
    UIImageView *titleImageView = [[UIImageView alloc]initWithImage:titleImage];
    titleImageView.frame = CGRectMake(0, 0, self.frame.size.width,self.frame.size.height);
    titleImageView.contentMode = UIViewContentModeScaleAspectFit;
    titleImageView.tag = self.tag;
    titleImageView.userInteractionEnabled = YES;
    [self addSubview:titleImageView];
    
    titleImageView.layer.borderWidth = 1.0;
    titleImageView.layer.borderColor = [[UIColor grayColor]CGColor];
    
    
    UIImageView *infoView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"TitleViewInfo.png"]];
    infoView.frame = CGRectMake(0, 0, 240, 250);
    infoView.center =self.center;
    infoView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:infoView];
    
    UILabel *recipeDescription = [[UILabel alloc] initWithFrame:CGRectMake(50, 70, 220, 100)];
    recipeDescription.backgroundColor = [UIColor clearColor];
    recipeDescription.textColor = [UIColor whiteColor];
    recipeDescription.center =self.center;
    recipeDescription.numberOfLines = 0;
    recipeDescription.textAlignment = NSTextAlignmentCenter;
    recipeDescription.text = description;
    
    [self addSubview:recipeDescription];
    
    
    NSLog(@"tag: %ldi", (long)self.tag);
    
    UILabel *menuForDay = [[UILabel alloc] initWithFrame:CGRectMake(0, 400, 320, 40)];
    menuForDay.backgroundColor = [UIColor whiteColor];
    menuForDay.textColor = [UIColor grayColor];
    menuForDay.font = [UIFont boldSystemFontOfSize:20];
    menuForDay.textAlignment = NSTextAlignmentCenter;
    menuForDay.text = (self.tag)?@"Tomorrow's Menu": @"Today's Menu";
    
    [titleImageView addSubview:menuForDay];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
