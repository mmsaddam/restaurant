//
//  AppDelegate.h
//  Restaurant
//
//  Created by Hafizur Rahman on 6/29/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSONKit.h"
#import "UserProfile.h"
#import "Registration.h"
#import "ItemDetails.h"
#import "CartItem.h"

#define HOST_URL @"http://bonapp.mx/Bonnapp/"
//#define appDelegate (AppDelegate*)[[UIApplication sharedApplication]delegate]


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) NSMutableArray *cartArray;
@property (strong, nonatomic) NSMutableArray *foodArray;


- (void)addToCart:(ItemDetails*)foodItem;
- (void)removeFromCart:(ItemDetails*)foodItem;
@end
