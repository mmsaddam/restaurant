//
//  SignInViewController.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "SignInViewController.h"
#import "PassForgotViewController.h"
#import "AppDelegate.h"


@interface SignInViewController ()

@property(nonatomic, strong) IBOutlet UITextField *emailTextFleid;

@property(nonatomic, strong) IBOutlet UITextField *passwordTextFleid;

@property (nonatomic, strong) NSMutableData *resultData;

@property (nonatomic, strong) NSURLConnection *urlConnection;

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *indicatorView;
@end

@implementation SignInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    if (self.urlConnection)
        [self.urlConnection cancel];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Sign In";

    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submitSignInDetailsPressed)];
    
}

- (IBAction)forgotPasswordButtonTapped:(id)sender{

    PassForgotViewController *passForgotViewC = [[PassForgotViewController alloc]initWithNibName:@"PassForgotViewController" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:passForgotViewC animated:YES];
}

- (void)submitSignInDetailsPressed{

    
    if ([self.emailTextFleid.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email address field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (![self stringIsValidEmail:self.emailTextFleid.text]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email address is not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    NSLog(@"pass: %@", self.passwordTextFleid.text);
    
    if ([self.passwordTextFleid.text length] <= 0){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Password field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    // Call for authentication
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@authentication.php?Email=%@",HOST_URL,self.emailTextFleid.text];
    
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:1];
    
    if (self.urlConnection)
        [self.urlConnection cancel];
    
    self.urlConnection = [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]] delegate:self];
    
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [self.indicatorView startAnimating];
}

- (void)checkForAthenticationAfterGettingDataFromServer:(NSDictionary*)dictionary{

    NSString *userPass = @"";
    
    if ([dictionary objectForKey:@"Password"])
        userPass = [dictionary objectForKey:@"Password"];
    
    if ([userPass isEqualToString:self.passwordTextFleid.text]){
    
        // Already Authenticated User, so get all of the users data
        
        UserProfile *user = [[UserProfile alloc]init];
        user.name = [dictionary objectForKey:@"UserName"];
        user.email = [dictionary objectForKey:@"Email"];
        user.isNewsLetter = [[dictionary objectForKey:@"IsNewsLetter"] boolValue];
        user.creditCardNo = [dictionary objectForKey:@"CreditCardNo"];
        user.balance = [dictionary objectForKey:@"Balance"];
        user.phone = [dictionary objectForKey:@"Phone"];
        user.registeredBy = (RegisteredService)[[dictionary objectForKey:@"RegisteredBy"] integerValue];
        user.gender = [dictionary objectForKey:@"Gender"];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:user ];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:kUserAccountDetailsKey];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}



- (BOOL) stringIsValidEmail:(NSString *)checkString
{
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


#pragma mark - NSURLConnection Delegate


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    _resultData = [[NSMutableData alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [_resultData appendData:data];
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Connection" message:@"No internet connection and/or server didn't responding" delegate:nil cancelButtonTitle:@"Try Again" otherButtonTitles:nil];
    [alert show];
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
    [self.indicatorView stopAnimating];

}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    NSString *resultString = [[NSString alloc]initWithData:_resultData encoding:1];
    
    NSDictionary *resultDic = [resultString objectFromJSONString];
    
    NSLog(@"%@", resultDic);
    
    if ([resultDic objectForKey:@"result"]){
        
        [self performSelectorOnMainThread:@selector(checkForAthenticationAfterGettingDataFromServer:) withObject:[resultDic objectForKey:@"result"] waitUntilDone:NO];
        
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Registration" message:@"Email address already exist, Please try in different email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
    [self.indicatorView stopAnimating];

}


#pragma mark UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{

    NSLog(@"textFieldDidBeginEditing tag %li", (long)textField.tag);
}


- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{

    NSLog(@"textFieldShouldEndEditing tag %li", (long)textField.tag);

    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    NSLog(@"textFieldShouldReturn tag %li", (long)textField.tag);
    
    if (textField.tag == 100)
        [self.passwordTextFleid becomeFirstResponder];
    
    else if (textField.tag == 101){
        
        [self submitSignInDetailsPressed];
    }

    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
