//
//  PhoneVC.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "PhoneVC.h"
#import "AppDelegate.h"
#import "PasswordVC.h"
#import "PhoneNumberFormatter.h"

@interface PhoneVC ()

@property (nonatomic, strong) IBOutlet UITextField *phoneTextField;
@property (nonatomic, strong) PhoneNumberFormatter *formatter;
@end

@implementation PhoneVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Phone";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(submitSignInDetailsPressed)];
    
    self.formatter = [[PhoneNumberFormatter alloc]init];
    
    [self.phoneTextField becomeFirstResponder];

}


- (void)submitSignInDetailsPressed{
    
    if ([self.phoneTextField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Phone Number field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if ([self.phoneTextField.text length]< 10){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Phone Number is not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }

    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsTempKey];
    UserProfile *user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    user.phone = self.phoneTextField.text;
    
    NSData *newData = [NSKeyedArchiver archivedDataWithRootObject:user ];
    [[NSUserDefaults standardUserDefaults] setObject:newData forKey:kUserAccountDetailsTempKey];
    
    PasswordVC *nameVC = [[PasswordVC alloc]initWithNibName:@"PasswordVC" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:nameVC animated:YES];

}
//es_MX

- (IBAction)textFieldValueChanged:(id)sender{

    
    NSLog(@"Text: %@", self.phoneTextField.text);
    
    self.phoneTextField.text = [self.formatter format:self.phoneTextField.text withLocale:@"us"];
}

#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self submitSignInDetailsPressed];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{

    
    NSLog(@"Text: %@", self.phoneTextField.text);

    self.phoneTextField.text = [self.formatter format:self.phoneTextField.text withLocale:@"es_MX"];
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
