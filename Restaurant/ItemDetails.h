//
//  ItemDetails.h
//  Restaurant
//
//  Created by Hafizur Rahman on 7/6/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemDetails : NSObject
@property (nonatomic) NSInteger itemNo;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *price;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *availableTime;

- (void)showDetails;
@end
