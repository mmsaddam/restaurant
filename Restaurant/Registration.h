//
//  Registration.h
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface Registration : NSObject

@property (nonatomic) id delegate;
@property (nonatomic) SEL selector;

- (void)initWithUserProfile:(UserProfile*)user forCallbackSelector:(SEL)sel forDelegate:(id)del;

@end
