//
//  EmailVC.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "EmailVC.h"
#import "AppDelegate.h"
#import "PhoneVC.h"

@interface EmailVC ()

@property (nonatomic, strong) IBOutlet UITextField *emailTextField;

@end

@implementation EmailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Email";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(submitSignInDetailsPressed)];

    [self.emailTextField becomeFirstResponder];
}


- (void)submitSignInDetailsPressed{
    
    
    if ([self.emailTextField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email address field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (![self stringIsValidEmail:self.emailTextField.text]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email address is not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    

    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kUserAccountDetailsTempKey];
    UserProfile *user = (UserProfile *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
    user.email = self.emailTextField.text;
    
    NSData *newData = [NSKeyedArchiver archivedDataWithRootObject:user ];
    [[NSUserDefaults standardUserDefaults] setObject:newData forKey:kUserAccountDetailsTempKey];

    
    PhoneVC *nameVC = [[PhoneVC alloc]initWithNibName:@"PhoneVC" bundle:[NSBundle mainBundle]];
    [self.navigationController pushViewController:nameVC animated:YES];
}


- (BOOL) stringIsValidEmail:(NSString *)checkString
{
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}



#pragma mark UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [self submitSignInDetailsPressed];
        
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
