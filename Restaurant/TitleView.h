//
//  TitleView.h
//  Restaurant
//
//  Created by Hafizur Rahman on 7/5/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleView : UIView

- (void)titleWithImage:(UIImage*)titleImage withDescription:(NSString*)description;
@end
