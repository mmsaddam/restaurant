//
//  Registration.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "Registration.h"


@interface Registration ()

@property (nonatomic, strong) NSMutableData *resultData;
@property (nonatomic) RegisteredService service;
@end

@implementation Registration


- (void)initWithUserProfile:(UserProfile*)user forCallbackSelector:(SEL)sel forDelegate:(id)del{

    self.delegate = del;
    self.selector = sel;
    
    
    // Register to Internet Server
    NSString* urlFormat = @"%@insert.php?UserName=%@&&Email=%@&&Password=%@&&CreditCardNo=%@&&Phone=%@&&Balance=%@&&RegisteredBy=%d&&IsNewsLetter=%d&&IsAdmin=0&&Gender=%@";
    
    [user showUserProfile];
    
    NSString* urlString = [NSString stringWithFormat:urlFormat,HOST_URL, user.name, user.email,user.password ,user.creditCardNo,user.phone,user.balance ,user.registeredBy,user.isNewsLetter,user.gender];
    
    _service = user.registeredBy;
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:1];
    
    [NSURLConnection connectionWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]] delegate:(!_service)?self:nil];
}

#pragma mark - NSURLConnection Delegate


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    _resultData = [[NSMutableData alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [_resultData appendData:data];
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Connection" message:@"No internet connection and/or server didn't responding" delegate:nil cancelButtonTitle:@"Try Again" otherButtonTitles:nil];
    [alert show];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    NSString *resultString = [[NSString alloc]initWithData:_resultData encoding:1];
    
    NSDictionary *resultDic = [resultString objectFromJSONString];
    
    [self.delegate performSelectorOnMainThread:self.selector withObject:resultDic waitUntilDone:NO];
    
    NSLog(@"%@", resultDic);
    
    if ([[resultDic objectForKey:@"result"] boolValue]){
        
        NSLog(@"Successfull inserted");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Registration Successful!" message:@"Enjoy with Bonapp foods" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
    else{
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error in Registration" message:@"Email address already exist, Please try in different email address" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

@end

