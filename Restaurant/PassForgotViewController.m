//
//  PassForgotViewController.m
//  Restaurant
//
//  Created by Hafizur Rahman on 7/3/14.
//  Copyright (c) 2014 RTC Hubs Limited. All rights reserved.
//

#import "PassForgotViewController.h"
#import "AppDelegate.h"

@interface PassForgotViewController ()
@property (nonatomic, strong) IBOutlet UITextField *emailTextField;

@end

@implementation PassForgotViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"Reset Password";
    
    
     self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"Submit" style:UIBarButtonItemStylePlain target:self action:@selector(submitSignInDetailsPressed)];
}

- (void)submitSignInDetailsPressed{

    if ([self.emailTextField.text length]<= 0){ // Checking for empty string
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email address field is empty" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    if (![self stringIsValidEmail:self.emailTextField.text]){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Email address is not valid" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    // Call url here
    
}

- (BOOL) stringIsValidEmail:(NSString *)checkString
{
    
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
