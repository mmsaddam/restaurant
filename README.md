##Online Restaurant 
========================

[![Platform](http://img.shields.io/badge/platform-ios-blue.svg?style=flat
)](https://developer.apple.com/iphone/index.action)
[![License](http://img.shields.io/badge/license-MIT-lightgrey.svg?style=flat
)](http://mit-license.org)

![sample](screenshots/img1.png)
![sample](screenshots/img2.png)
![sample](screenshots/img3.png)
![sample](screenshots/img4.png)

##Language

Objective-C

##Tools
Xcode 6.3, Cocoa Touch

##Requirement
iOS 7
Only for iPhone 5, iPod Touch 5G
iPhone 6, iPhone 6 Plus coming soon...
 

##About The App
This a online restaurant management app for iOS. User can checkout the daily food menu with corresponding price. User can sign up the app using his facebook or mail account. User can order menu on online and pay cash on delivary. The user interface of the app is very nice so that any one will loves it.  

## Contributing

Forks, patches and other feedback are welcome.

## Creator

[Muzahidul Islam](http://mmsaddam.github.io/) 
[Blog](http://mmsaddam.github.io/)

## License

Online Restaurant is available under the MIT license. See the LICENSE file for more info.